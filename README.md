此示例演示使用DatagramSocket类来进行客户端和服务器端通信设计。该示例的客户端组件创建一个套接字，使用套接字发送和接收数据，并关闭套接字。

源码来源：http://code.msdn.microsoft.com/windowsapps/DatagramSocket-sample-76a7d82b

首先是效果图（访问weibo的结果），然后是几个核心代码，最后是源码